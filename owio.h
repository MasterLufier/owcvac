/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef OWIO_H
#define OWIO_H

#include <QObject>
#include <QFile>
#include <QUrl>
#include <QVariant>
#include "owdata.h"
#include <QXmlStreamReader>
#include <QDataStream>
#include <QDate>

#include <QDebug>

class OWIO : public QObject
{
    Q_OBJECT
    Q_PROPERTY(OWData * data READ data WRITE setData NOTIFY dataChanged)
private:
    OWData * m_data;

public:
    explicit OWIO(QObject *parent = nullptr);
    //Load any compatible .csv or .sdb2
    Q_INVOKABLE OWData * loadFile(const QString url);
    //Save to Wireless WorkBench format
    Q_INVOKABLE bool saveCSVWWB(const QString url);
    //TODO: Save SDB2 file
    Q_INVOKABLE bool saveSDB2(const QString url);
    //Save to Wireless System Manager
    Q_INVOKABLE bool saveWSM(const QString url);

    Q_INVOKABLE QByteArray readFile(const QString url);

    OWData * parseWSM(QByteArray ba, OWData * data);
    OWData * parseWWB(QByteArray ba, OWData * data);
    OWData * parseSDB2(QByteArray ba, OWData * data);
    OWData * data() const;

signals:

    void dataChanged(OWData * data);

public slots:
void setData(OWData * data);
void mergeData(OWData * data);
};

#endif // OWIO_H
