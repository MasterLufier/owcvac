/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "owio.h"

OWIO::OWIO(QObject *parent) : QObject(parent)
{
    m_data = new OWData(this);
}

OWData * OWIO::loadFile(const QString url)
{
    //Load File
    OWData * data = new OWData(this);
    QByteArray ba = readFile(url);
    //Check file format
    //WSM
    if(ba.split(';').first() == "Receiver")
    {
        data = parseWSM(ba, data);
        data->WSMtoWWB();
    }
    //WWB CSV
    else if (ba.contains(".") && ba.contains(","))
    {
        data = parseWWB(ba, data);
        data->WWBtoWSM();
    }
    //WWB SDB2
    else if (ba.contains("scan_data_source"))
    {
        data = parseSDB2(ba, data);
        data->WWBtoWSM();
    }
    else
    {
        qWarning() << "Error Format";
        return nullptr;
    }
    mergeData(data);
    return data;
}

QByteArray OWIO::readFile(const QString url)
{
    QByteArray ba;
    QFile file(url);
    if (file.open(QIODevice::ReadOnly)) {
        ba = file.readAll();
        file.close();
    }
    return ba;
}

bool OWIO::saveCSVWWB(const QString url)
{
    QFile file(url);
    if (file.open(QFile::WriteOnly | QFile::Truncate) && data() != nullptr)
    {
        QTextStream out(&file);
        foreach (auto p, data()->frequencyData()) {
            out << QString::number(p.x(),'f',3) << ',' << QString::number(p.y(),'f',2) << "\r\n";
        }
        return true;
    }
    return false;
}

bool OWIO::saveSDB2(const QString url)
{
    QFile file(url);
    if (file.open(QFile::WriteOnly | QFile::Truncate) && data() != nullptr)
    {
        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();
        stream.writeStartElement("scan_data_source");
        QString ver;
        stream.writeAttribute("ver", OWWWBHeader::stringFromVer(data()->WWBHeader()->scanDataSource().ver));
        stream.writeAttribute("id", data()->WWBHeader()->scanDataSource().id);
        stream.writeAttribute("model", data()->WWBHeader()->scanDataSource().model);
        stream.writeAttribute("name", data()->WWBHeader()->scanDataSource().name);
        stream.writeAttribute("device_id", data()->WWBHeader()->scanDataSource().device_id);
        stream.writeAttribute("device_type_id", data()->WWBHeader()->scanDataSource().device_type_id);
        stream.writeAttribute("band", data()->WWBHeader()->scanDataSource().band);
        stream.writeAttribute("mode", data()->WWBHeader()->scanDataSource().mode);
        QLocale locale = QLocale("en_US");
        QString localeDate = locale.toString(data()->WWBHeader()->scanDataSource().date, "ddd MMM dd yyyy");
        stream.writeAttribute("date", localeDate);
        stream.writeAttribute("time", data()->WWBHeader()->scanDataSource().time.toString("hh:mm:ss.zzz"));
        stream.writeAttribute("color", data()->WWBHeader()->scanDataSource().color.name(QColor::HexRgb));
        //
        stream.writeStartElement("data_sets");
        stream.writeAttribute("count", QString::number(data()->WWBHeader()->dataSets().count));
        stream.writeAttribute("no_data_value",QString::number(data()->WWBHeader()->dataSets().no_data_value));
        //
        stream.writeStartElement("freq_set");
        for(int i = 0; i<data()->frequencyData().count(); ++i)
        {
            stream.writeStartElement("f");
            stream.writeCharacters(QString::number(data()->frequencyData().at(i).x()*1000));
            stream.writeEndElement();
        }
        stream.writeEndElement();
        //
        stream.writeStartElement("data_set");
        stream.writeAttribute("index",QString::number(data()->WWBHeader()->dataSet().index));
        stream.writeAttribute("freq_units",data()->WWBHeader()->dataSet().freq_units);
        stream.writeAttribute("ampl_units",data()->WWBHeader()->dataSet().ampl_units);
        stream.writeAttribute("start_freq", QString::number(data()->WWBHeader()->dataSet().start_freq,'f',1));
        stream.writeAttribute("stop_freq", QString::number(data()->WWBHeader()->dataSet().stop_freq,'f',1));
        stream.writeAttribute("step_freq", QString::number(data()->WWBHeader()->dataSet().step_freq, 'f', 3));
        stream.writeAttribute("res_bandwidth", data()->WWBHeader()->dataSet().res_bandwidth);
        stream.writeAttribute("scale_factor", QString::number(data()->WWBHeader()->dataSet().scale_factor));
        stream.writeAttribute("date", localeDate);
        stream.writeAttribute("time", data()->WWBHeader()->dataSet().time.toString("hh:mm:ss.zzz"));
        stream.writeAttribute("date_time", QString::number(data()->WWBHeader()->dataSet().date_time.toMSecsSinceEpoch()));
        for(int i = 0; i<data()->frequencyData().count(); ++i)
        {
            stream.writeStartElement("v");
            stream.writeCharacters(QString::number(data()->frequencyData().at(i).y(),'f',1));
            stream.writeEndElement();
        }

        stream.writeEndElement();
        stream.writeEndElement();
        //TODO: Markers
        stream.writeStartElement("markers");
        stream.writeEndElement();
        //
        stream.writeEndDocument();
        return true;
    }
    return false;
}

bool OWIO::saveWSM(const QString url)
{
    QFile file(url);
    if (file.open(QFile::WriteOnly | QFile::Truncate) && data() != nullptr)
    {
        //take frequency range and step size
        int startFreq = data()->frequencyData().first().x()*1000;
        int endFreq = data()->frequencyData().last().x()*1000;
        int stepSize = data()->frequencyData().at(1).x()*1000-startFreq;
        QTextStream out(&file);
        out << "Receiver" << ";" << data()->WSMHeader()->reciver() << "\r\n";
        out << "Date/Time" << ";" << data()->WSMHeader()->dateTime().toString("dd-MM-yyyy hh-mm-ss AP") << "\r\n";
        out << "RFUnit" << ";" << data()->WSMHeader()->rfUnit() << "\r\n";
        out << "Owner" << ";" << "\r\n";
        out << "ScanCity" << ";" << "\r\n";
        out << "ScanComment" << ";" << "\r\n";
        out << "ScanCountry" << ";" << "\r\n";
        out << "ScanDescription" << ";" << "\r\n";
        out << "ScanInteriorExterior" << ";" << "\r\n";
        out << "ScanLatitude" << ";" << "\r\n";
        out << "ScanLongitude" << ";" << "\r\n";
        out << "ScanName" << ";" << "\r\n";
        out << "ScanPostalCode" << ";" << "\r\n";
        out << "\r\n";
        out << "Frequency Range [kHz]" << ";" << startFreq << ";" << endFreq << ";" << stepSize  << "\r\n";
        out << "Frequency;RF level (%);RF level" << "\r\n";
        foreach (auto p, data()->frequencyData()) {
            out << QString::number(p.x()*1000,'f',0) << ";;" << p.y()+107 << "\r\n";
        }
        return true;
    }
    return false;
}

OWData *OWIO::parseWSM(QByteArray ba, OWData * data)
{
    QTextStream s(&ba);
    //1 Receiver
    data->WSMHeader()->setReciver(s.readLine().split(';').last().toInt());
    //2 Date/Time
    qInfo() << s.readLine();
    //3 RFUnit;
    data->WSMHeader()->setRfUnit(s.readLine().split(';').last());
    //    * 4 Owner;<int>                                //1
    data->WSMHeader()->setOwner(s.readLine().split(';').last().toInt());
    //    * 5 ScanCity;<string>                          //moscow
    qInfo() << s.readLine();
    //    * 6 ScanComment;<string>                       //
    qInfo() << s.readLine();
    //    * 7 ScanCountry;<string>                       //
    qInfo() << s.readLine();
    //    * 8 ScanDescription;<string>                   //
    qInfo() << s.readLine();
    //    * 9 ScanInteriorExterior;<string>              //Indoor with high isolation
    qInfo() << s.readLine();
    //    * 10 ScanLatitude;                             //
    qInfo() << s.readLine();
    //    * 11 ScanLongitude;                            //
    qInfo() << s.readLine();
    //    * 12 ScanName;<string>                         //asb3
    qInfo() << s.readLine();
    //    * 13 ScanPostalCode;<int>                      //
    qInfo() << s.readLine();
    //    * 14 <empty string>                            // empty
    qInfo() << s.readLine();
    //    * 15 Frequency Range [kHz];<int>;<int>;<int>   //470000;638000;25 //without delimiter
    QStringList l = s.readLine().split(";");
    data->WSMHeader()->setStartFreq(l.at(1).toFloat());
    data->WSMHeader()->setStopFreq(l.at(2).toFloat());
    data->WSMHeader()->setStepFreq(l.at(3).toFloat());
    //    * 16 Frequency;RF level (%);RF level
    qInfo() << s.readLine();

    while(!s.atEnd()) {
        QString line = s.readLine();

        //Take freq.
        float freq = line.split(';').first().toFloat();
        //add separator
        freq = freq/1000;
        //take gain
        float gain = line.split(';').last().toFloat();
        //convert to dBm
        gain = gain-107;
        data->appendFrequencyData(freq,gain);
    }
    data->WSMtoWWB();
    return data;
}

OWData *OWIO::parseWWB(QByteArray ba, OWData *data)
{
    QTextStream s(&ba);
    while(!s.atEnd())
    {
        QString line = s.readLine();
        //Take freq.
        float freq = line.split(',').first().toFloat();
        //take gain
        float gain = line.split(',').last().toFloat();
        data->appendFrequencyData(freq,gain);
    }
    data->WWBtoWSM();
    return data;
}

OWData *OWIO::parseSDB2(QByteArray ba, OWData *data)
{
    QXmlStreamReader xml(ba);

    while (xml.readNextStartElement())
    {
        if (xml.name() == "scan_data_source")
        {
            //            qDebug() << "scan_data_source";
            OWWWBHeader::Scan_data_source scanData;
            QStringList l = xml.attributes().value("ver").toString().split(".");
            //            qDebug() << "ver=" << xml.attributes().value("ver") ;
            scanData.ver = OWWWBHeader::fillVer(l);
            //            qDebug() << "id=" <<  xml.attributes().value("id") ;
            scanData.id = xml.attributes().value("id").toString() ;
            //            qDebug() << "model=" << xml.attributes().value("model");
            scanData.model = xml.attributes().value("model").toString();
            //            qDebug() << "name=" << xml.attributes().value("name");
            scanData.name = xml.attributes().value("name").toString();
            //            qDebug() << "device_id=" << xml.attributes().value("device_id");
            scanData.device_id = xml.attributes().value("device_id").toString();
            //            qDebug() << "device_type_id=" << xml.attributes().value("device_type_id");
            scanData.device_type_id = xml.attributes().value("device_type_id").toString();
            //            qDebug() << "band=" << xml.attributes().value("band");
            scanData.band  = xml.attributes().value("band").toString();
            //            qDebug() << "mode=" << xml.attributes().value("mode");
            scanData.mode = xml.attributes().value("mode").toString();
            //            qDebug() << "date=" << xml.attributes().value("date");
            scanData.date = QLocale("en_US").toDate(xml.attributes().value("date").toString().simplified(), "ddd MMM dd yyyy");
            //            qDebug() << "time=" << xml.attributes().value("time");
            scanData.time = QTime::fromString(xml.attributes().value("time").toString(), "hh:mm:ss.zzz");
            //            qDebug() << "color=" << xml.attributes().value("color");
            scanData.color = QColor(xml.attributes().value("color").toString());
            data->WWBHeader()->setScanDataSource(scanData);
            while (xml.readNextStartElement())
            {
                if(xml.name() == "data_sets")
                {
                    //                    qDebug() << "data_sets";
                    OWWWBHeader::Data_sets dataSets;
                    //                    qDebug() << "count=" << xml.attributes().value("count");
                    dataSets.count = xml.attributes().value("count").toInt();
                    //                    qDebug() << "no_data_value=" << xml.attributes().value("no_data_value");
                    dataSets.no_data_value = xml.attributes().value("no_data_value").toInt();

                    data->WWBHeader()->setDataSets(dataSets);
                    while (xml.readNextStartElement())
                    {
                        if(xml.name() == "freq_set")
                        {
                            //                            qDebug() << xml.name() << "freq_set";
                            while (xml.readNextStartElement())
                            {
                                if(xml.name() == "f")
                                {
                                    //                                    qDebug() << "text:" << xml.readElementText();
                                    float freq = xml.readElementText().toFloat()/1000;
                                    data->appendFrequencyData(freq,0);
                                }
                            }
                        }
                        else if (xml.name() == "data_set")
                        {
                            //                            qDebug() << "data_set";
                            OWWWBHeader::Data_set dataSet;
                            //                            qDebug() << "index=" << xml.attributes().value("index");
                            dataSet.index = xml.attributes().value("index").toInt();
                            //                            qDebug() << "freq_units=" << xml.attributes().value("freq_units");
                            dataSet.freq_units = xml.attributes().value("freq_units").toString();
                            //                            qDebug() << "ampl_units=" << xml.attributes().value("ampl_units");
                            dataSet.ampl_units = xml.attributes().value("ampl_units").toString();
                            //                            qDebug() << "start_freq=" << xml.attributes().value("start_freq");
                            dataSet.start_freq = xml.attributes().value("start_freq").toFloat();
                            //                            qDebug() << "stop_freq=" << xml.attributes().value("stop_freq");
                            dataSet.stop_freq = xml.attributes().value("stop_freq").toFloat();
                            //                            qDebug() << "step_freq=" << xml.attributes().value("step_freq");
                            dataSet.step_freq = xml.attributes().value("step_freq").toFloat();
                            //                            qDebug() << "res_bandwidth=" << xml.attributes().value("res_bandwidth");
                            dataSet.res_bandwidth = xml.attributes().value("res_bandwidth").toString();
                            //                            qDebug() << "scale_factor=" << xml.attributes().value("scale_factor");
                            dataSet.scale_factor = xml.attributes().value("scale_factor").toInt();
                            //                            qDebug() << "date=" << xml.attributes().value("date");
                            dataSet.date = scanData.date;
                            //                            qDebug() << "time=" << xml.attributes().value("time");
                            dataSet.time = scanData.time;
                            //                            qDebug() << "date_time=" << xml.attributes().value("date_time");
                            dataSet.date_time = QDateTime::fromMSecsSinceEpoch(xml.attributes().value("date_time").toLongLong());

                            data->WWBHeader()->setDataSet(dataSet);
                            //list iterator
                            int i = 0;
                            while (xml.readNextStartElement())
                            {
                                if (xml.name() =="v")
                                {
                                    //                                    qDebug() << "text:" << xml.readElementText();
                                    float gain = xml.readElementText().toFloat();
                                    data->setGain(i,gain);
                                }
                                ++i;
                            }
                        }
                    }
                }
            }
        }
    }
    if (xml.hasError())
    {
        qWarning() << "Error read SDB2 File";
    }
    data->WWBtoWSM();
    return data;
}

OWData *OWIO::data() const
{
    return m_data;
}

void OWIO::setData(OWData *data)
{
    if (m_data == data)
        return;

    m_data = data;
    emit dataChanged(m_data);
}

void OWIO::mergeData(OWData *data)
{
    if(m_data->frequencyData().isEmpty())
    {
        setData(data);
        return;
    }
    m_data->mergeFrequencyDataList(data->frequencyData());
    emit dataChanged(m_data);
}
