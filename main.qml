/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12
import QtQuick.Window 2.2
import Qt.labs.platform 1.0
import Owcvac 1.0

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 800
    height: 600
    minimumWidth: 800
    minimumHeight: 400

    title: qsTr("Open Wireless Coordination Viewer And Converter")
    //
    ColumnLayout {
        id:mainLayout
        anchors.fill: parent

        RowLayout {
            id:buttonBar
            Layout.fillWidth: true
            Layout.margins: 5
            Button{
                id:openFile
                Layout.fillWidth: true
                Layout.minimumWidth: background.width
                text: "Import CSV or SDB2"
                onClicked: {
                    openDialog.visible = true
                }
            }
            Button {
                id:clearData
                Layout.fillWidth: true
                Layout.minimumWidth: background.width
                text: "Clear Data"
                onClicked: {
                    pIO.data.clearData(mainChart.series);
                }
            }

            Rectangle{
                id:spacer
                Layout.fillWidth: true
                Layout.preferredWidth: applicationWindow.width
            }

            Button{
                id:exportWSM
                Layout.fillWidth: true
                Layout.minimumWidth: background.width
                text: "Export CSV to WSM"
                onClicked: {
                    saveWSM.visible = true
                }
            }
            Button {
                id:exportCSVWWB
                Layout.fillWidth: true
                Layout.minimumWidth: background.width
                text: "Export CSV to WWB"
                onClicked: {
                    saveCSVWWB.visible = true
                }
            }
            Button {
                id:exportSDB2WWB
                Layout.fillWidth: true
                Layout.minimumWidth: background.width
                text: "Export SDB2 to WWB"
                onClicked: {
                    saveSDB2WWB.visible = true
                }
            }
        }
        RowLayout{
            Layout.fillWidth: true
            Layout.fillHeight: true
            ColumnLayout{
                Layout.fillHeight: true
                Layout.margins: 5
                Button{
                    id: plusMaxGain
                    Layout.maximumWidth: background.implicitWidth/3
                    text: "+"
                    autoRepeat: true
                    onClicked:
                    {
                        pIO.data.setMaxGain(pIO.data.maxGain+1)
                    }
                }
                Button{
                    id: minusMaxGain
                    Layout.maximumWidth: background.implicitWidth/3
                    text: "-"
                    autoRepeat: true
                    onClicked:
                    {
                        if(pIO.data.maxGain>pIO.data.minGain+1)
                            pIO.data.setMaxGain(pIO.data.maxGain-1)
                    }
                }
                Rectangle{
                    Layout.fillHeight: true
                }
                Button{
                    id: plusMinGain
                    Layout.maximumWidth: background.implicitWidth/3
                    text: "+"
                    autoRepeat: true
                    onClicked:
                    {
                        if(pIO.data.minGain<pIO.data.maxGain-1)
                            pIO.data.setMinGain(pIO.data.minGain+1)
                    }
                }
                Button{
                    id: minusMinGain
                    Layout.maximumWidth: background.implicitWidth/3
                    text: "-"
                    autoRepeat: true
                    onClicked:
                    {
                            pIO.data.setMinGain(pIO.data.minGain-1)
                    }
                }
            }

            MainChart {
                id:mainChart
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }

    FileDialog{
        id: openDialog
        title: "Please choose a file"
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.OpenFile
        nameFilters:["Wirless Data Files (*.csv *.sdb2)", "All Files (*)"]

        onAccepted: {
            //***********
            var path = openDialog.currentFile.toString();
            if(path == "")
                return;
            // replace prefixed "file:///" to "/"
            path = path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"/");
            // unescape html codes like '%23' for '#'
            var cleanPath = decodeURIComponent(path);
            /////
            pIO.loadFile(cleanPath);
            this.visible = false
        }
        onRejected: {
            console.log("Canceled")
            this.visible = false
        }
        Component.onCompleted: visible = false
    }

    FileDialog{
        id: saveCSVWWB
        title: "Please choose a file"
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.SaveFile
        nameFilters:["Coma Separated Files (*.csv)", "All Files (*)"]

        onAccepted: {
            //***********
            var path = saveCSVWWB.currentFile.toString();

            // replace prefixed "file:///" to "/"
            path = path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"/");
            // unescape html codes like '%23' for '#'
            var cleanPath = decodeURIComponent(path);
            /////
            pIO.saveCSVWWB(cleanPath);
            this.visible = false
        }
        onRejected: {
            console.log("Canceled")
            this.visible = false
        }
        Component.onCompleted: visible = false
    }

    FileDialog{
        id: saveSDB2WWB
        title: "Please choose a file"
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.SaveFile
        nameFilters:["Coma Separated Files (*.sdb2)", "All Files (*)"]

        onAccepted: {
            //***********
            var path = saveSDB2WWB.currentFile.toString();

            // replace prefixed "file:///" to "/"
            path = path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"/");
            // unescape html codes like '%23' for '#'
            var cleanPath = decodeURIComponent(path);
            /////
            pIO.saveSDB2(cleanPath);
            this.visible = false
        }
        onRejected: {
            console.log("Canceled")
            this.visible = false
        }
        Component.onCompleted: visible = false
    }

    FileDialog{
        id: saveWSM
        title: "Please choose a file"
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.SaveFile
        nameFilters:["Coma Separated Files (*.csv)", "All Files (*)"]

        onAccepted: {
            //***********
            var path = saveWSM.currentFile.toString();

            // replace prefixed "file:///" to "/"
            path = path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"/");
            // unescape html codes like '%23' for '#'
            var cleanPath = decodeURIComponent(path);
            /////
            pIO.saveWSM(cleanPath);
            this.visible = false
        }
        onRejected: {
            console.log("Canceled")
            this.visible = false
        }
        Component.onCompleted: visible = false
    }
}
