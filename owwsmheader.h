/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef OWWSMHEADER_H
#define OWWSMHEADER_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QGeoCoordinate>
/* WSM Header format:
 * 1 Receiver;<int>
 * 2 Date/Time;<QDateTime>                      //<DD-MM-YYYY HH-MM-SS PM(AM)
 * 3 RFUnit;<string>                            //dB
 * 4 Owner;<int>                                //1
 * 5 ScanCity;<string>                          //moscow
 * 6 ScanComment;<string>                       //
 * 7 ScanCountry;<string>                       //
 * 8 ScanDescription;<string>                   //
 * 9 ScanInteriorExterior;<string>              //Indoor with high isolation
 * 10 ScanLatitude;                             //
 * 11 ScanLongitude;                            //
 * 12 ScanName;<string>                         //asb3
 * 13 ScanPostalCode;<int>                      //
 * 14 <empty string>                            // empty
 * 15 Frequency Range [kHz];<int>;<int>;<int>   //470000;638000;25 //in kHz
 * 16 Frequency;RF level (%);RF level
 */

class OWWSMHeader : public QObject
{
    Q_OBJECT
    //
    Q_PROPERTY(int reciver READ reciver WRITE setReciver NOTIFY reciverChanged)
    Q_PROPERTY(QDateTime dateTime READ dateTime WRITE setDateTime NOTIFY dateTimeChanged)
    Q_PROPERTY(QString rfUnit READ rfUnit WRITE setRfUnit NOTIFY rfUnitChanged)
    Q_PROPERTY(int owner READ owner WRITE setOwner NOTIFY ownerChanged)
    Q_PROPERTY(QString scanCity READ scanCity WRITE setScanCity NOTIFY scanCityChanged)
    Q_PROPERTY(QString scanComment READ scanComment WRITE setScanComment NOTIFY scanCommentChanged)
    Q_PROPERTY(QString scanCountry READ scanCountry WRITE setScanCountry NOTIFY scanCountryChanged)
    Q_PROPERTY(QString scanDescription READ scanDescription WRITE setScanDescription NOTIFY scanDescriptionChanged)
    Q_PROPERTY(QString scanInteriorExterior READ scanInteriorExterior WRITE setScanInteriorExterior NOTIFY scanInteriorExteriorChanged)
    //TODO:Geo coordinate
    Q_PROPERTY(QString scanName READ scanName WRITE setScanName NOTIFY scanNameChanged)
    Q_PROPERTY(int scanPostalCode READ scanPostalCode WRITE setScanPostalCode NOTIFY scanPostalCodeChanged)
    Q_PROPERTY(float startFreq READ startFreq WRITE setStartFreq NOTIFY startFreqChanged)
    Q_PROPERTY(float stopFreq READ stopFreq WRITE setStopFreq NOTIFY stopFreqChanged)
    Q_PROPERTY(float stepFreq READ stepFreq WRITE setStepFreq NOTIFY stepFreqChanged)

    int m_reciver = 0;
    QDateTime m_dateTime = QDateTime::currentDateTimeUtc();
    QString m_rfUnit = "dB";
    int m_owner = 0;
    QString m_scanCity = "moscow";
    QString m_scanComment;
    QString m_scanCountry = "Russia";
    QString m_scanDescription;
    QString m_scanInteriorExterior = "Indoor with high isolation";
    QString m_scanName = "asb3";
    int m_scanPostalCode;
    float m_startFreq;
    float m_stopFreq;
    float m_stepFreq;

public:
    explicit OWWSMHeader(QObject *parent = nullptr);

    int reciver() const;
    QDateTime dateTime() const;
    QString rfUnit() const;
    int owner() const;
    QString scanCity() const;
    QString scanComment() const;
    QString scanCountry() const;
    QString scanDescription() const;
    QString scanInteriorExterior() const;
    QString scanName() const;
    int scanPostalCode() const;
    float startFreq() const;
    float stopFreq() const;
    float stepFreq() const;

signals:

    void reciverChanged(int reciver);
    void dateTimeChanged(QDateTime dateTime);
    void rfUnitChanged(QString rfUnit);
    void ownerChanged(int owner);
    void scanCityChanged(QString scanCity);
    void scanCommentChanged(QString scanComment);
    void scanCountryChanged(QString scanCountry);
    void scanDescriptionChanged(QString scanDescription);
    void scanInteriorExteriorChanged(QString scanInteriorExterior);
    void scanNameChanged(QString scanName);
    void scanPostalCodeChanged(int scanPostalCode);
    void startFreqChanged(float startFreq);
    void stopFreqChanged(float stopFreq);
    void stepFreqChanged(float stepFreq);

public slots:
    void setReciver(int reciver);
    void setDateTime(QDateTime dateTime);
    void setRfUnit(QString rfUnit);
    void setOwner(int owner);
    void setScanCity(QString scanCity);
    void setScanComment(QString scanComment);
    void setScanCountry(QString scanCountry);
    void setScanDescription(QString scanDescription);
    void setScanInteriorExterior(QString scanInteriorExterior);
    void setScanName(QString scanName);
    void setScanPostalCode(int scanPostalCode);
    void setStartFreq(float startFreq);
    void setStopFreq(float stopFreq);
    void setStepFreq(float stepFreq);
};

#endif // OWWSMHEADER_H
