/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "owwsmheader.h"

OWWSMHeader::OWWSMHeader(QObject *parent) : QObject(parent)
{

}

int OWWSMHeader::reciver() const
{
    return m_reciver;
}

QDateTime OWWSMHeader::dateTime() const
{
    return m_dateTime;
}

QString OWWSMHeader::rfUnit() const
{
    return m_rfUnit;
}

int OWWSMHeader::owner() const
{
    return m_owner;
}

QString OWWSMHeader::scanCity() const
{
    return m_scanCity;
}

QString OWWSMHeader::scanComment() const
{
    return m_scanComment;
}

QString OWWSMHeader::scanCountry() const
{
    return m_scanCountry;
}

QString OWWSMHeader::scanDescription() const
{
    return m_scanDescription;
}

QString OWWSMHeader::scanInteriorExterior() const
{
    return m_scanInteriorExterior;
}

QString OWWSMHeader::scanName() const
{
    return m_scanName;
}

int OWWSMHeader::scanPostalCode() const
{
    return m_scanPostalCode;
}

float OWWSMHeader::startFreq() const
{
    return m_startFreq;
}

float OWWSMHeader::stopFreq() const
{
    return m_stopFreq;
}

float OWWSMHeader::stepFreq() const
{
    return m_stepFreq;
}

void OWWSMHeader::setReciver(int reciver)
{
    if (m_reciver == reciver)
        return;

    m_reciver = reciver;
    emit reciverChanged(m_reciver);
}

void OWWSMHeader::setDateTime(QDateTime dateTime)
{
    if (m_dateTime == dateTime)
        return;

    m_dateTime = dateTime;
    emit dateTimeChanged(m_dateTime);
}

void OWWSMHeader::setRfUnit(QString rfUnit)
{
    if (m_rfUnit == rfUnit)
        return;

    m_rfUnit = rfUnit;
    emit rfUnitChanged(m_rfUnit);
}

void OWWSMHeader::setOwner(int owner)
{
    if (m_owner == owner)
        return;

    m_owner = owner;
    emit ownerChanged(m_owner);
}

void OWWSMHeader::setScanCity(QString scanCity)
{
    if (m_scanCity == scanCity)
        return;

    m_scanCity = scanCity;
    emit scanCityChanged(m_scanCity);
}

void OWWSMHeader::setScanComment(QString scanComment)
{
    if (m_scanComment == scanComment)
        return;

    m_scanComment = scanComment;
    emit scanCommentChanged(m_scanComment);
}

void OWWSMHeader::setScanCountry(QString scanCountry)
{
    if (m_scanCountry == scanCountry)
        return;

    m_scanCountry = scanCountry;
    emit scanCountryChanged(m_scanCountry);
}

void OWWSMHeader::setScanDescription(QString scanDescription)
{
    if (m_scanDescription == scanDescription)
        return;

    m_scanDescription = scanDescription;
    emit scanDescriptionChanged(m_scanDescription);
}

void OWWSMHeader::setScanInteriorExterior(QString scanInteriorExterior)
{
    if (m_scanInteriorExterior == scanInteriorExterior)
        return;

    m_scanInteriorExterior = scanInteriorExterior;
    emit scanInteriorExteriorChanged(m_scanInteriorExterior);
}

void OWWSMHeader::setScanName(QString scanName)
{
    if (m_scanName == scanName)
        return;

    m_scanName = scanName;
    emit scanNameChanged(m_scanName);
}

void OWWSMHeader::setScanPostalCode(int scanPostalCode)
{
    if (m_scanPostalCode == scanPostalCode)
        return;

    m_scanPostalCode = scanPostalCode;
    emit scanPostalCodeChanged(m_scanPostalCode);
}

void OWWSMHeader::setStartFreq(float startFreq)
{
    if (qFuzzyCompare(m_startFreq, startFreq))
        return;

    m_startFreq = startFreq;
    emit startFreqChanged(m_startFreq);
}

void OWWSMHeader::setStopFreq(float stopFreq)
{
    if (qFuzzyCompare(m_stopFreq, stopFreq))
        return;

    m_stopFreq = stopFreq;
    emit stopFreqChanged(m_stopFreq);
}

void OWWSMHeader::setStepFreq(float stepFreq)
{
    if (qFuzzyCompare(m_stepFreq, stepFreq))
        return;

    m_stepFreq = stepFreq;
    emit stepFreqChanged(m_stepFreq);
}
