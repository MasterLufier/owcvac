/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "owdata.h"

OWData::OWData(QObject *parent) : QObject(parent)
{
    m_WSMHeader = new OWWSMHeader(this);
    m_WWBHeader = new OWWWBHeader(this);
}

QList<QPointF> OWData::frequencyData() const
{
    return m_frequencyData;
}

OWWSMHeader *OWData::WSMHeader() const
{
    return m_WSMHeader;
}

void OWData::appendFrequencyData(float freq, float gain)
{
    appendFrequencyData(QPointF(freq,gain));
}

void OWData::appendFrequencyData(QPointF p)
{
    m_frequencyData.append(p);
    if(p.y() < minGain())
        setMinGain(p.y()-10);
    if(p.y() > maxGain())
        setMaxGain(p.y()+10);
    emit frequencyDataChanged(m_frequencyData);
}

void OWData::prependFrequencyData(float freq, float gain)
{
    prependFrequencyData(QPointF(freq,gain));
}

void OWData::prependFrequencyData(QPointF p)
{
    m_frequencyData.prepend(p);
    if(p.y() < minGain())
        setMinGain(p.y()-10);
    if(p.y() > maxGain())
        setMaxGain(p.y()+10);
    emit frequencyDataChanged(m_frequencyData);
}

void OWData::insertFrequencyData(int pos, float freq, float gain)
{
    insertFrequencyData(pos, QPointF(freq, gain));
}

void OWData::insertFrequencyData(int pos, QPointF p)
{
    m_frequencyData.insert(pos, p);
    if(p.y() < minGain())
        setMinGain(p.y()-10);
    if(p.y() > maxGain())
        setMaxGain(p.y()+10);
    emit frequencyDataChanged(m_frequencyData);
}

void OWData::replaceFrequencyData(float freq, float gain)
{
    replaceFrequencyData(QPointF(freq, gain));
}

void OWData::replaceFrequencyData(QPointF p)
{
    if(p.x() < frequencyData().first().x()){
        prependFrequencyData(p);
    }
    else if (p.x() > frequencyData().last().x()) {
        appendFrequencyData(p);
    }
    else {
        for (int i = 0; i< m_frequencyData.length(); ++i) {
            if(qFuzzyCompare(frequencyData().at(i).x(), p.x()))
            {
                setGain(i,p.y());
                return;
            }
        }
    }
}

void OWData::mergeFrequencyData(QPointF p)
{
    if(p.x() < frequencyData().first().x()){
        prependFrequencyData(p);
        return;
    }

    else if (p.x() > frequencyData().last().x()) {
        appendFrequencyData(p);
        return;
    }

    else
    {
        for (int i = 0; i< m_frequencyData.length(); ++i) {
            if(qFuzzyCompare(frequencyData().at(i).x(), p.x()))
            {
                if(abs(frequencyData().at(i).y()-p.y()) < 10)
                    return;
                float oldValue, newValue, powA, powB;
                oldValue = frequencyData().at(i).y();
                newValue = p.y();
                powA = 1/(10 * powf(10, (oldValue*-0.1f)-1.0f));//pow(10, oldValue*0.1f);
                powB = 1/(10 * powf(10, (newValue*-0.1f)-1.0f));
                //merge gain
                float newGain = 10*log10f((powA+powB));
                setGain(i,newGain);
                //                qDebug() << oldValue << newValue << newGain;
                return;
            }
            if(frequencyData().at(i).x() > p.x())
            {
                insertFrequencyData(i, p);
                return;
            }
        }
    }
}

void OWData::mergeFrequencyData(float freq, float gain)
{
    mergeFrequencyData(QPointF(freq,gain));
}

void OWData::mergeFrequencyDataList(QList<QPointF> l)
{
    foreach (auto v, l) {
        mergeFrequencyData(v);
    }
}

void OWData::setFrequency(int index, float freq){
    if(m_frequencyData.count() > index)
    {
        m_frequencyData[index].setX(freq);
    }
    else
    {
        m_frequencyData.insert(index, QPointF(freq,-200));
    }
}

void OWData::setFrequency(float freq)
{
    QPointF p;
    p.setX(freq);
    m_frequencyData.append(p);
}

void OWData::setGain(int index, float gain){
    if(m_frequencyData.count() > index)
    {
        m_frequencyData[index].setY(gain);
        if(gain < minGain())
            setMinGain(gain-10);
        if(gain > maxGain())
            setMaxGain(gain+10);
    }
    else
    {
        QPointF p;
        p.setY(gain);
        m_frequencyData.insert(index,p);
        if(gain < minGain())
            setMinGain(gain-10);
        if(gain > maxGain())
            setMaxGain(gain+10);
    }
}

void OWData::clearData(QLineSeries *series){
    m_frequencyData.clear();
    series->clear();
}

void OWData::addSeries(QLineSeries *series)
{
    series->clear();
    series->append(m_frequencyData);
}

void OWData::setMinGain(float minGain)
{
    if (qFuzzyCompare(m_minGain, minGain))
        return;

    m_minGain = minGain;
    emit minGainChanged(m_minGain);
}

void OWData::setMaxGain(float maxGain)
{
    if (qFuzzyCompare(m_maxGain, maxGain))
        return;

    m_maxGain = maxGain;
    emit maxGainChanged(m_maxGain);
}

OWWWBHeader *OWData::WWBHeader() const
{
    return m_WWBHeader;
}

void OWData::WWBtoWSM()
{
    //Load possible data
    m_WSMHeader->setDateTime(m_WWBHeader->dataSet().date_time);
    m_WSMHeader->setStartFreq(m_WWBHeader->dataSet().step_freq);
    m_WSMHeader->setStopFreq(m_WWBHeader->dataSet().stop_freq);
    m_WSMHeader->setStepFreq(m_WWBHeader->dataSet().step_freq);
}

void OWData::WSMtoWWB()
{
    OWWWBHeader::Data_set d = m_WWBHeader->dataSet();
    d.date = m_WSMHeader->dateTime().date();
    d.time = m_WSMHeader->dateTime().time();
    d.date_time = m_WSMHeader->dateTime();

    d.start_freq = m_WSMHeader->startFreq();
    d.stop_freq = m_WSMHeader->stopFreq();
    d.step_freq = m_WSMHeader->stepFreq();

    m_WWBHeader->setDataSet(d);
}

float OWData::minGain() const
{
    return m_minGain;
}

float OWData::maxGain() const
{
    return m_maxGain;
}

void OWData::setFrequencyData(QList<QPointF> frequencyData)
{
    if (m_frequencyData == frequencyData)
        return;

    m_frequencyData = frequencyData;
    emit frequencyDataChanged(m_frequencyData);
}

void OWData::setWSMHeader(OWWSMHeader *WSMHeader)
{
    if (m_WSMHeader == WSMHeader)
        return;

    m_WSMHeader = WSMHeader;
    emit WSMHeaderChanged(m_WSMHeader);
}

void OWData::setWWBHeader(OWWWBHeader *WWBHeader)
{
    if (m_WWBHeader == WWBHeader)
        return;

    m_WWBHeader = WWBHeader;
    emit WWBHeaderChanged(m_WWBHeader);
}
