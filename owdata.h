/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef OWDATA_H
#define OWDATA_H

#include <QObject>
#include <QMap>
#include <QVariant>
#include <QPointF>
#include <QDebug>
#include <QtMath>
#include <QtCharts>
#include "owwsmheader.h"
#include "owwwbheader.h"

QT_CHARTS_USE_NAMESPACE

class OWData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(OWWSMHeader * WSMHeader READ WSMHeader WRITE setWSMHeader NOTIFY WSMHeaderChanged)
    Q_PROPERTY(OWWWBHeader * WWBHeader READ WWBHeader WRITE setWWBHeader NOTIFY WWBHeaderChanged)
    Q_PROPERTY(float minGain READ minGain WRITE setMinGain NOTIFY minGainChanged)
    Q_PROPERTY(float maxGain READ maxGain WRITE setMaxGain NOTIFY maxGainChanged)
    //
public:
    explicit OWData(QObject *parent = nullptr);
    QList<QPointF> frequencyData() const;
    OWWSMHeader * WSMHeader() const;
    OWWWBHeader * WWBHeader() const;
    //Make WSM Header from WWB Header
    void WWBtoWSM();
    //Make WWB header from WSM Header
    void WSMtoWWB();
    float minGain() const;
    float maxGain() const;

private:
    QList<QPointF> m_frequencyData;
    OWWSMHeader * m_WSMHeader;
    OWWWBHeader * m_WWBHeader;
    float m_minGain = -80;
    float m_maxGain = -70;

signals:

    void frequencyDataChanged(QList<QPointF> frequencyData);
    void WSMHeaderChanged(OWWSMHeader * WSMHeader);
    void WWBHeaderChanged(OWWWBHeader * WWBHeader);
    void minGainChanged(float minGain);
    void maxGainChanged(float maxGain);

public slots:
    void setFrequencyData(QList<QPointF> frequencyData);
    void setWSMHeader(OWWSMHeader * WSMHeader);
    void setWWBHeader(OWWWBHeader * WWBHeader);
    void appendFrequencyData(float freq, float gain);
    void appendFrequencyData(QPointF p);
    void prependFrequencyData(float freq, float gain);
    void prependFrequencyData(QPointF p);
    void insertFrequencyData(int pos, float freq, float gain);
    void insertFrequencyData(int pos, QPointF p);
    void replaceFrequencyData(float freq, float gain);
    void replaceFrequencyData(QPointF p);
    void mergeFrequencyData(QPointF p);
    void mergeFrequencyData(float freq, float gain);
    void mergeFrequencyDataList(QList<QPointF> l);
    void setFrequency (int index, float freq);
    void setFrequency (float freq);
    void setGain(int index, float gain);
    void clearData(QLineSeries * series);
    void addSeries(QLineSeries * series);
    void setMinGain(float minGain);
    void setMaxGain(float maxGain);
};

#endif // OWDATA_H
