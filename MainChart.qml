/*
This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtCharts 2.3

ChartView {
    id:chart
    antialiasing: true
    property alias series: lineSeries
    legend.visible: false;

    Component.onCompleted: {
        axisY.min = pIO.data.minGain
        axisY.max = pIO.data.maxGain
    }

    Connections {
        target: pIO
        onDataChanged:
        {
            pIO.data.addSeries(lineSeries)
            axisX.min = lineSeries.at(0).x
            axisX.max = lineSeries.at(lineSeries.count-1).x

            axisY.min = pIO.data.minGain
            axisY.max = pIO.data.maxGain
        }
    }
    Connections{
        target: pIO.data
        onMaxGainChanged: axisY.max = pIO.data.maxGain
        onMinGainChanged: axisY.min = pIO.data.minGain
    }

    ValueAxis {
        id: axisY
        min: -120
        max: 20
        tickCount: 15
    }

    ValueAxis {
        id: axisX
        min: 470
        max: 640
        tickCount: 10
    }

    LineSeries {
        id: lineSeries
        axisX: axisX
        axisY: axisY
    }
}
