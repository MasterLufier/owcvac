#ifndef OWWWBHEADER_H
#define OWWWBHEADER_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QColor>
#include <QDataStream>
#include <QDebug>

class OWWWBHeader : public QObject
{
    Q_OBJECT
    //
public:
    struct Ver{
        int A = 0;
        int B = 0;
        int C = 0;
        int D = 1;
    };

    struct Scan_data_source
    {
        Ver ver;
        QString id = "{ac5f46d1-6313-4d89-9c63-df910cb95314}";
        QString model = "QLXD4";
        QString name = "1";
        QString device_id = "CCEAC054-E139-11DF-84BA-0015C5F3F612";
        QString device_type_id = "CCEAC054-E139-11DF-84BA-0015C5F3F612";
        QString band = "K51";
        QString mode = "";
        QDate date = QDate::currentDate();
        QTime time = QTime::currentTime();
        QColor color = "#00FF00";
        friend QDebug operator<<(QDebug dbg, const Scan_data_source &type)
        {
            dbg.nospace() << type.id
                          << type.model;
            return dbg.maybeSpace();
        }
    };

    struct Data_sets
    {
        int count = 0;
        int no_data_value = -140;
    };

    struct Data_set
    {
        int index = 0;
        QString freq_units = "Khz";
        QString ampl_units = "dBm";
        float start_freq = 606000.0;
        float stop_freq = 670000.0;
        float step_freq = 25.000;
        QString res_bandwidth = "TODO";
        int scale_factor = 1;
        QDate date = QDate::currentDate();
        QTime time = QTime::currentTime();
        QDateTime date_time = QDateTime::currentDateTimeUtc();
    };

private:
    Scan_data_source m_scanDataSource;
    Data_sets m_dataSets;
    Data_set m_dataSet;

public:
    explicit OWWWBHeader(QObject *parent = nullptr);
    Q_PROPERTY(Scan_data_source scanDataSource READ scanDataSource WRITE setScanDataSource NOTIFY scanDataSourceChanged)
    Q_PROPERTY(Data_sets dataSets READ dataSets WRITE setDataSets NOTIFY dataSetsChanged)
    Q_PROPERTY(Data_set dataSet READ dataSet WRITE setDataSet NOTIFY dataSetChanged)

    Scan_data_source scanDataSource() const
    {
        return m_scanDataSource;
    }

    Data_sets dataSets() const
    {
        return m_dataSets;
    }

    Data_set dataSet() const
    {
        return m_dataSet;
    }

    inline static Ver fillVer(QStringList l){
        Ver out;
        out.A = l.takeFirst().toInt();
        out.B = l.takeFirst().toInt();
        out.C = l.takeFirst().toInt();
        out.D = l.takeFirst().toInt();
        return out;
    }

    inline static QString stringFromVer(Ver ver)
    {
        QString out;
        out.append(QString::number(ver.A))
                .append(".")
                .append(QString::number(ver.B)
                        .append("."))
                .append(QString::number(ver.C)
                        .append(".")
                        .append(QString::number(ver.D)));
        return out;
    }
signals:

    void scanDataSourceChanged(Scan_data_source scanDataSource);
    void dataSetsChanged(Data_sets dataSets);
    void dataSetChanged(Data_set dataSet);

public slots:
    void setScanDataSource(Scan_data_source scanDataSource)
    {
        if (m_scanDataSource.id == scanDataSource.id
                && m_scanDataSource.model == scanDataSource.model
                && m_scanDataSource.name == scanDataSource.name
                && m_scanDataSource.device_id == scanDataSource.device_id
                && m_scanDataSource.device_type_id == scanDataSource.device_type_id
                && m_scanDataSource.band == scanDataSource.band
                && m_scanDataSource.mode == scanDataSource.mode
                && m_scanDataSource.date == scanDataSource.date
                && m_scanDataSource.time == scanDataSource.time
                && m_scanDataSource.color == scanDataSource.color)
            return;

        m_scanDataSource = scanDataSource;
        emit scanDataSourceChanged(m_scanDataSource);
    }
    void setDataSets(Data_sets dataSets)
    {
        if (m_dataSets.count == dataSets.count
                && m_dataSets.no_data_value == dataSets.no_data_value)
            return;

        m_dataSets = dataSets;
        emit dataSetsChanged(m_dataSets);
    }
    void setDataSet(Data_set dataSet)
    {
        if (m_dataSet.index == dataSet.index
                && m_dataSet.freq_units == dataSet.freq_units
                && m_dataSet.ampl_units == dataSet.ampl_units
                && qFuzzyCompare(m_dataSet.start_freq, dataSet.start_freq)
                && qFuzzyCompare(m_dataSet.stop_freq, dataSet.stop_freq)
                && qFuzzyCompare(m_dataSet.step_freq, dataSet.step_freq)
                && m_dataSet.res_bandwidth == dataSet.res_bandwidth
                && m_dataSet.scale_factor == dataSet.scale_factor
                && m_dataSet.date == dataSet.date
                && m_dataSet.time == dataSet.time
                && m_dataSet.date_time == dataSet.date_time)
            return;

        m_dataSet = dataSet;
        emit dataSetChanged(m_dataSet);
    }
};

#endif // OWWWBHEADER_H
