This file is part of OWCVAC - Open Wireless Coordination Viewer And Converter.
Copyright (C) 2018  Mikhail Ivanov. masluf@gmail.com

OWCVAC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OWCVAC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OWCVAC.  If not, see <https://www.gnu.org/licenses/>.

OWCVAC - Open Wireless Coordination Viewer And Converter.

This software is make for view, merge and convert scan files is exported from Shure Wireless Workbench and Sennheiser Wireless System Manager.

Version 0.0.1: Initial Version
- Add import WSM and WWB .CSV files
- Add save .CSV files for WSM and WWB format

Version 0.0.2:
- add import and export .SDB2 files from WWB
- add default WWB header
- add important part of WSM header
- some bug fix

Version 1.0.0:
- add merging when open several files
- add graphical view opening files

Copyright (C) 2018-2019  Mikhail Ivanov. masluf@gmail.com
